#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "color.h"

int custom_random(int n) {
  char data;
  FILE *fp;
  fp = fopen("/dev/urandom", "r");
  fread(&data, 1, 1, fp);
  fclose(fp);
  return ((unsigned int)data) % n;
}

int game(int borne, int somme, int mise, int* nb_tour, int* nb_victoire) {
  int verbose = 0;

  int current_mise = mise;

  int c = 0;

  for(int n = 0; n < borne; n++) {

    *nb_tour = *nb_tour + 1;

    if ((c = custom_random(36)) > 18) {

      *nb_victoire = *nb_victoire + 1;

      if (verbose) printf("Victoire ! ");

      somme += current_mise;
      current_mise = mise;
    } else if(c == 0){
      somme -= mise / 2;
    } else {
      if (verbose) printf("Défaite ! ");
      if ((somme -  current_mise) <= 0) {
        printf(RED "C'est loose !, mise : %d, nb tour : %d\n", current_mise / 2, n);
        break;
      }
      somme -= current_mise;
      current_mise *= 2;
    }

    if (verbose) printf("Balance: %d €\n", somme);
  }

  return somme;
}

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    // Counter
    int nb_victoire = 0;
    int nb_tour = 0;

    // Params
    int borne , s_start , mise, nb_game;

    // Gains
    int gain = 0, capital = 0;
    int s_final = 0;

    char input[32];

    printf(CYAN "===== Le bon simulateur =====\n");

    printf(GREEN "Nombre de games\n");
    scanf("%d", &nb_game);

    printf(GREEN "Nombre de lancés\n");
    scanf("%d", &borne);

    printf(GREEN "Somme de départ\n");
    scanf("%d", &s_start);

    printf(GREEN "Montant des mises\n");
    scanf("%d", &mise);

    printf(CYAN "===== Debut des games =====\n");

  for(int n = 0; n < nb_game; n ++) {
    capital = game(borne, s_start, mise, &nb_tour, &nb_victoire);
    gain = capital - s_start;
    printf(YELLOW "Gains de la game : %d €\n", gain);
    s_final += gain;
  }

  printf(CYAN "===== Résultats =====\n");
  printf(BLUE "Gains :  %d €\n", s_final);
  printf("Nombre de tours: %d, nombre de victoire %d\n", nb_tour, nb_victoire);
  printf("Probabilité de réussite de tour: %f %% \n", (float)nb_victoire / (float)nb_tour * 100);
}
